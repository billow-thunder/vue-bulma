# Upgrading from 2.1.x

Vue Bulma 3 has several breaking changes compared to 2.1, hence the version bump. To a large degree, the 3.0 API is the same as that of 2.1 and even 2.0. Whilst primarily targeted at new apps, this is a recommended upgrade for priority production apps. For a short while, 2.1.x will continue to be maintained on the `legacy/2.1` branch.

To get started, update the dependency:

```shell
npm i @billow/vue-bulma@3
```

Next, review and make the required changes below, and optionally add in any new goodies listed in the section that follows.

## REQUIRED CHANGES

### Stylesheet References

3.0 no longer uses SASS, but rather SCSS. If you find you’re receiving compile errors related to the main stylesheet, make sure you’re importing `~@billow/vue-bulma/styles` and **not** `~@billow/vue-bulma/styles.sass`

---
### Modals

In 2.1, the Alert and Confirm modals were added, but you still had to install `vue-modal-dialogs` and register the `$alert` and `$confirm` prototypes yourself. In 3.0, this is **no longer necessary**, as the `vue-modal-dialogs` package has been forked into Vue Bulma, effectively making it a first-party submodule. As a result, the heavy lifting is now done on your behalf, and we’ll continue to maintain the package internally going forward.

#### Remove Prototypes

So, the first step here is to remove the prototype registrations you had before (in, say `@/modals`, or wherever you might have been registering your modals):

```diff
- import Alert from '@billow/vue-bulma/modals/Alert'
- import Confirm from '@billow/vue-bulma/modals/Confirm'
- Vue.prototype.$alert = create(Alert)
- Vue.prototype.$confirm = create(Confirm)
```

#### create() is now useModal()

Next, the `create` function, previously imported from the third-party package, has been renamed to `useModal`, and can be imported from `@billow/vue-bulma` directly. The signature of the method has not changed, so all your modal definitions will continue to work as normal. If you don’t happen to like the new name of this method, you can always alias it: `import { useModal as create } from '@billow/vue-bulma`.

```diff
- import { create } from 'vue-modal-dialogs'
+ import { useModal } from '@billow/vue-bulma'

- export const myModal = create(MyModal)
+ export const myModal = useModal(MyModal)
```

#### Remove the dependency

At this point, it’s safe to `npm uninstall vue-modal-dialogs`

#### Changes to the Modal component

##### Component renamed

As part of giving modals first-class support, the `BaseModal` component has been renamed to `Modal`. A simple global search and replace (replace `base-modal` with `modal`) will handle this upgrade easily.

##### The Close Button

In 2.x, the position of the close button was determind by a prop called `close-button`, which accepted either `inner` or `outer`. This has prop has been changed in 3.0 to `close-button-outside`, which is now a `Boolean`. Make sure any references you have are changd accordingly.

##### Footer Class

In 2.x, the flex layout for the footer was set to `flex-start`. In 3.0, this has been changed to `is-reversed`, which means the buttons will appear on the right hand side of the footer, instead of the left. If you’d like to fall back to the left, simply add `footer-class="false"` to your modal.

---
### Some dependencies were removed

The dependencies below were removed, either because they're not being used, or because they were not removed after a component/utility removal in a previous 2.1 release.

1. `bulma-tooltip`, which has been replaced with `v-tooltip` (see the next section to upgrade).
2. `bulma-pricingtable`, for which the component was removed in a previous release.
3. `bulma-divider`
4. `bulma-slider`, as well as the accompanying `SliderInput`. This may be re-instated at some point, using an in-house implementation.
5. `bulma-badge`, along with the accompanying `badge` props.
6. `@billow/js-helpers` [peer: no action required], as only the `Pager` component was using it for inflection (now handled internally).

For each of these, you’ll either need to upgrade, or remove implementations that depended on these packages.

---
### Tooltips

The tooltips mechanism in 3.0 has had a major overhaul, given that the behaviour of `bulma-tooltip` was quite opinionated and a tad buggy, and lacked customization options desired in several projects. This overhaul involves swapping out the old dependency with [v-tooltip](https://github.com/Akryum/v-tooltip), a package by [Guillaume Chau](https://github.com/Akryum) that leverages `tooltip.js`, which in turn extends `popper.js`, something we already use for our dropdown-based components.

Given the removal of `bulma-tooltip`, any references to the `tooltip` and `is-tooltip-*` classes will no longer do anything. Likewise, `data-tooltip` is also gone.

In its place is the `v-tooltip` directive, which provides much more customization capabilities. Here’s an example of how you’d swap out a simple tooltip:

```diff
- <action-button class="tooltip" data-tooltip="This is a Button.">My Button</action-button>
+ <action-button v-tooltip="'This is a button'">My Button</action-button>
```

Additional classs-based customizations are now handled in the directive itself. This is done by passing in an object instead of a string:

```diff
- <action-button
-   class="tooltip is-tooltip-left is-tooltip-info"
-   data-tooltip="This is a Button.">
-   My Button
- </action-button>
+ <action-button
+   v-tooltip="{
+     content: 'This is a Button',
+     placement: 'left',
+     classes: 'is-info'
+   }">
+   My Button
+ </action-button>
```

> 💡 Field labels now use the new tooltips as well, which means you might want to revise any `hint-class` props you might be using. For example, you’ll want to change `is-tooltip-danger` to `is-danger`

#### Supported Classes / Additional Customization

The following classes are supported:

- `is-primary`
- `is-info` and `is-info-light`
- `is-link` and `is-link-light`
- `is-warning` and `is-warning-light`
- `is-danger` and `is-danger-light`

If you’d like to add in some more, simply add the following to a stylesheet somewhere in your app:

```scss
.tooltip-container {
  &.is-custom {
    .tooltip-inner {
      background: $custom;
      color: findColorInvert($custom);
      // You can also change the border radius, font-size, padding, and anything else you think might make it look awesome.
    }

    .tooltip-arrow {
      border-color: $custom;
    }
  }
}
```

For reference, here are the initial variables used to style tooltips. You can change these after importing `@billow/vue-bulma/scss/initial-variables`:

```scss
$tooltip-index: 39 !default;
$tooltip-background-color: $scheme-invert !default;
$tooltip-color: $scheme-main !default;
$tooltip-radius: $radius-small !default;
$tooltip-padding: .35em .75em !default;
$tooltip-font-size: $size-7 !default;
```

And lastly, `v-tooltip` accepts **all** customizations shown in the [repo’s readme](https://github.com/Akryum/v-tooltip). If you’d like to make your customizations affect all tooltips, app-wide, then you can pass in those same customizations to the `tooltip` property when instantiating `VueBulma`:

> ⚠ **Do not** change `defaultTemplate` or `popover.defaultBaseClass`, as the stylesheets rely on these.

```js
Vue.use(VueBulma, {
  config: {
    tooltip: {
      // your global customizations
    }
  }
})
```

---
### Slots are now scoped by default

Any slots used in components are now scoped, which simply means that the internal references have changed from `$slots` to `$scopedSlots`. This means that you can be 100% sure that the new syntax will work everywhere:

```vue
<template #item="{ item }">
  <!-- stuff goes here -->
</template>
```

---
### Minor Pager Changes

The `Pager` component has received some minor upgrades.

Firstly, the static button that contained the 'details' of the pagination state retained the default static button appearance in 2.x. By default in 3.0, it inherits the dedicated `is-scheme-main` class, which defaults to white. This makes the pager a little lighter, and more unified in light and dark modes. This, however, can be changed by adding the `details-class="[]"` prop to the component.

Secondly, the details format itself has been changed from to wrap the page numbers in bold. In 3.0, the default for the `format` prop is now `Page *:current* of *:last* (:total :totalContext)`. The asterisks are converted to span tags with a class of `has-text-weight-bold`, using the `basicTags` import from `@billow/vue-bulma/utilities/string`, which can be used elsewhere in your project (see new goodies below).

And lastly, the default `context` has changed from `record` to `item`, and the default `working-text` has changed from `Please wait, loading data…` to just `Loading…`, for brevity.

## NEW GOODIES

That’s all in terms of what *needs* to be changed in order for 3.0 to function correctly in an existing app.

Next, here’s a list of things that are new or fixed in this release:

### Bulma was upgraded

In this release, Bulma has been bumped to 0.8. The stylesheet now leverages much of the changes made, such as using `$scheme-main` and `$scheme-invert` instead of `$white` and `$black`. This means that dark-mode support is now baked into VueBulma (sans a few oversights, which will be picked up on).

---
### DataSelector got some fixes and upgrades

Given that there are still some significant features and support layers to be added to the data selector, its still classified as a beta component. That said, it has received some significant bug fixes and performance improvements. Notably, these include the following:

- The computed items are no longer actually bound to component computation. Rebuilds of this data are now triggered manually, whether the incoming `items` change, the user searches for something, or the user selects something and the items are set to disappear when selected (this is the default behaviour). By doing this, we can better control how the list behaves in different scenarios.

- The `autofocus`, `autocomplete`, and `role` props were added for enhanced accessibility.

- In search mode, the data selector is now debouncable, which means you can pass the `debounce` prop in, like you would with a text input.

#### Adding Tags

When using a `taggable` data selector, you can now accept new tags provided by the user. Listen for the `@tag` event to handle any server-side logic you need to handle, and then update whatever you’re binding to `items`, as well as the bound-model:

```vue
<data-selector
  multiple
  taggable
  @tag="addNewColor"
  v-model="selectedColors"
  :items="availableColors">
  Select or add one or more colors
</data-selector>
```

```js
async newColor(color) {
  await server.addColor(color)
  this.selectedColors.push({
    value: color,
    label: color
  })
  this.availableColors.push({
    value: color,
    label: color
  })
}
```

#### Async Searching

You can now perform server-side searching when the user types into the search field. This is achieved using a combination of the `async` prop (which simply skips internal fuzzy searches) and the `@search` event, which you can use to capture user input, perform a search on your server, and update the data bound to `items` (which can be empty at the onset – up to you).

The `debounce` prop is also very useful here, as you don’t want to make too many calls to your server or degrade performance during a search.

```vue
<data-selector
  async
  searchable
  :debounce="500"
  :items="countries"
  @search="search"
  v-bind="{ working }"
  v-model="country"
  label-key="name"
  value-key="alpha2Code">
  Select a Country
</data-selector>
```

```js
export default {
  data: () => ({
    countries: [],
    working: false,
  }),

  methods: {
    search(input) {
      // If you’re wanting to be results-driven, use an empty item list
      if (!input) {
        this.countries = []
        return
      }

      this.working = true
      server.findCountriesByInput(input, response => {
        this.countries = response.data
        this.working = false
      }, error => {
        console.error(error)
        this.working = false
      })
    }
  }
}
```

---
### Date Picker

The date picker can now accept a list of disabled or enabled dates, show week numbers, and handle a custom callback when a day element is drawn on the calendar, or when the picker is ready to be used.

#### Enabled or Disabled Dates

You can now pass an array of dates to enable or disable to the `enabled-dates` and `disabled-dates` props respectively. These props are simply passed to the underlying Flatpickr instance, which means there is no specific behaviour added on top of what the library provides.

```vue
<date-picker :disabled-dates="['2020-01-01', '2020-03-07']">Pick a Date</datepicker>
<date-picker :enabled-dates="['2020-01-01', '2020-03-07']">Pick a Date</datepicker>
```

#### Show Week Numbers

To show week numbers, simply add the `week-numbers` prop to the picker:

```vue
<date-picker week-numbers>Pick a Date</date-picker>
```

#### onDayCreate and onReady callbacks

These callbacks are passed directly to Flatpickr, which means there is no special behaviour added on top of what the library provides.

In a nutshell, the [onDayCreate](https://flatpickr.js.org/events/#ondaycreate) (bound as `:on-day-create`) callback allows you to make modifications to the element that gets drawn in the calendar. You could use this to add colour indicators for events, for example. The onReady callback is simply an additional action you can take once the calendar has moved to a 'ready state'.

---
### Avatar Presence, Identicons, Colorization and Personas

#### Presence Indicators

Avatars in 3.0 have gained the ability to indicate "presence", such as "available" or "meeting". When you use a presence indicator, a small dot will be drawn to the bottom right of the avatar in the applicable color:

```vue
<avatar
  :presence="user.presence"
  :name="user.full_name"
  :path="user.avatar_url"/>
```
The following presets are available, each of which correspond with a Bulma color class:

- `available`: `is-success`
- `busy`: `is-danger`
- `meeting`: `is-warning`
- `tentative`: `is-info`

You can also use `is-primary` and `is-black`.

To add your own, simply add the following to a stylesheet somewhere in your project:

```scss
.avatar .presence {
  &.is-orange {
    background-color: orange;
  }
}
```


#### Identicons, by Gravatar

Identicon support has been added to Avatars as well. If you’d like to use an identicon instead of the default fallback of showing initials, simply set the `identicon` prop to the user’s email address:

```vue
<avatar … :identicon="user.email"/>
```

#### Colorize Initials

You might want to add a splash of color to avatars that don’t have images, and only have access to the user’s full name. By default, a simply grey background is rendered. However, if you add the `colorize` prop, the user’s initials will be hashed and converted to a color, which means that color will never change, unless their initials change.

```vue
<avatar … colorize :name="user.full_name"/>
```

#### Personas

A new component in 3.0, `Persona` wraps `Avatar`, allowing you to add some basic information about a user. Presence indicators are also supported.

The persona component accepts a name, subtitle, avatar, identicon, size, and presence:

```vue
<persona
  :size="32"
  :name="user.full_name"
  :presence="user.presence"
  :subtitle="user.company.name"
  :avatar="user.avatar_url"
  :identicon="user.email"/>
```

---
### Popovers

As part of the migration to `v-tooltip`, popovers are now supported and also have a default set of styling. Given that the package is imported and instantiated directly, it has no default additional customization/tooling, and may be used exactly as-is. To learn more, see [popover component reference](https://github.com/Akryum/v-tooltip#popover-component-reference). In a nutshell however, here’s how you can setup a basic popover, with default Bulma-based styling:

```vue
<v-popover>
  <action-button>Quick Info</action-button>
  <template slot="popover">
    <p>This is a popover.</p>
    <action-button v-close-popover>Close</action-button>
  </template>
</v-popover>
```

---
### Tag component

To provide a simpler implmentation of a standard Bulma tag, you can now use the new `Tag` component to render an interactive tag:

```vue
<tag can-delete delete-button-class="is-danger">Vue Bulma</tag>
```

---
### Basic Text Formatting

Used internally by the Pager, a new utility named `basicTags` has been added to 3.0, and can be accessed via `import { basicTags } from '@billow/vue-bulma/utilities/string'`. This utility allows you to format text wrapped in asterisks, underscores, and tildes, converting such text to bold, italic and striked respectively.

As an example:

```
*text* becomes <span class="has-text-weight-bold">text</span>
_text_ becomes <span class="is-italic">text</span>
~text~ becomes <strike>text</strike>
```

To use, simply pass your input to `basicTags`:

```js
return basicTags(text)
```
