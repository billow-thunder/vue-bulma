import { ModalWrapper } from '../utilities/modal/wrapper'

let requireComponent = require.context('../components', true, /\w+\.vue$/)

let install = (app, exclusions = []) => {
  // Auto-install @/components
  requireComponent.keys().forEach(filename => {
    let requiredComponent = requireComponent(filename),
      component = requiredComponent.default || requiredComponent,
      name = component.name || filename.replace(/.*\/(\w+).vue$/g, '$1')

    if (exclusions.includes(name)) return
    app.component(name, component)
  })

  // Manual installation of other components
  app.component('modal-wrapper', ModalWrapper)
}

export default install
