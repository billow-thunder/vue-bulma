import VTooltip from 'v-tooltip'

let install = (app, options = {}) => {
  app.use(VTooltip, options.tooltip)
}

export default install
