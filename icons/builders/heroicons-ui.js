/**
 * Heroicons UI builder for VueBulma.
 * Generates a JS exporter for tree-shaking icons in VueBulma
 * @author Mike Rockett <mike@billow.co.za>
 * @license ISC
 */

const buildPack = require('./builder')

let output = 'heroicons',
    source = 'heroicons-ui/icons',
    headers = [
      'Heroicons UI by Steve Schoger',
      '@see https://github.com/sschoger/heroicons-ui'
    ],
    filenameReplacements = {
      'cheveron': 'chevron',
      'icon-x': 'icon-times',
      'icon-': ''
    },
    bodyReplacements = {
      'class="heroicon-ui"': ''
    }

buildPack(output, source, headers, filenameReplacements, bodyReplacements)
