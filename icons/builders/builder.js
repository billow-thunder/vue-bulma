/**
 * Icon builder for VueBulma.
 * Generates a JS exporter for tree-shaking icons in VueBulma.
 * @author Mike Rockett <mike@billow.co.za>
 * @license ISC
 */

const { SVG } = require('simple-svg-tools')
const camel = require('camelcase')
const { F_OK } = require('fs').constants
const fs = require('fs').promises
const glob = require('glob')
const path = require('path')

/**
 * Set the output file, unlinking it if it exists.
 * @param {String} file The base file name (no extension)
 */
let setOutputFile = file => {
  file = __dirname + `/../${file}.js`

  fs.access(file, F_OK).then(async error => {
    console.info(`${file} ${error ? 'does not exist; skipping unlink.' : 'exists; unlinking.'}`);
    if (!error) await fs.unlink(file).catch(error => console.log(
      error || `Unlinked: ${file}`
    ))
  })

  return file
}

/**
 * Build the icon pack.
 * @param {String} source The source directory, relative to node_modules
 * @param {Array} headers An array of headers to be added to the output file
 * @param {String} output The location of the output file, generated with setOutputFile
 * @param {Object} filenameReplacements Key-value pair for filename replacements
 * @param {Object} bodyReplacements Key-value pair for body replacements
 */
let buildPack = (output, source, headers, filenameReplacements = {}, bodyReplacements = {}) => {
  let pack = output
  output = setOutputFile(output)

  glob(__dirname + `/../../node_modules/${source}/*.svg`, {}, (error, files) => {
    if (error) throw error
    if (!files.length) throw Error(`Looks like there are no icons in ~${source}`)

    let headerData = ''
    headers.push(`Exported for Billow’s VueBulma on ${new Date}\n`)
    headers.forEach(header => headerData += `// ${header}\n`)

    fs.writeFile(output, headerData).then(async () => {
      for (let file in files) {
        file = files[file]
        let content = await fs.readFile(file, { encoding: 'utf8' })

        let name = file.replace('.svg', '').replace('/', '')
        for (let key in filenameReplacements) {
          name = name.replace(key, filenameReplacements[key])
        }

        let svg = new SVG(content)
        let body = svg.getBody().replace(/\s*\n\s*/g, '')
        for (let key in bodyReplacements) {
          body = body.replace(key, bodyReplacements[key])
        }

        name = path.basename(name)

        await fs.appendFile(output, `/* ${name} */\nexport let ${camel(name)} = '${body}'\n\n`)

        console.log(`${pack}: ${name}`)
      }
    })
  })
}

module.exports = buildPack
