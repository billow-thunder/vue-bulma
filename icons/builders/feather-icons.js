/**
 * Zondicon builder for VueBulma.
 * Generates a JS exporter for tree-shaking icons in VueBulma
 * @author Mike Rockett <mike@billow.co.za>
 * @license ISC
 */

const buildPack = require('./builder')

let output = 'feather-icons',
    source = 'feather-icons/dist/icons',
    headers = [
      'Feather Icons by Cole Bemis',
      '@see https://feathericons.com/'
    ],
    filenameReplacements = {
      'delete': 'backspace',
      'package': 'package-closed'
    }

buildPack(output, source, headers, filenameReplacements)
