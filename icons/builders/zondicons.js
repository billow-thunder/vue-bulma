/**
 * Zondicon builder for VueBulma.
 * Generates a JS exporter for tree-shaking icons in VueBulma
 * @author Mike Rockett <mike@billow.co.za>
 * @license ISC
 */

const buildPack = require('./builder')

let output = 'zondicons',
    source = 'zondicons',
    headers = [
      'Zondicons by Steve Schoger',
      '@see https://zondicons.com'
    ],
    filenameReplacements = {
      'cheveron': 'chevron',
      'star-full': 'star'
    }

buildPack(output, source, headers, filenameReplacements)
