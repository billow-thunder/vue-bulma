/**
 * Bytesize icon builder for VueBulma.
 * Generates a JS exporter for tree-shaking icons in VueBulma.
 * @author Mike Rockett <mike@billow.co.za>
 * @license ISC
 */

const buildPack = require('./builder')

let output = 'bytesize',
    source = 'bytesize-icons/dist/icons',
    headers = [
      'Bytesize Icons by Dan Klammer',
      '@see https://github.com/danklammer/bytesize-icons'
    ],
    filenameReplacements = {
      'import': 'importer',
      'export': 'exporter'
    }

buildPack(output, source, headers, filenameReplacements)
