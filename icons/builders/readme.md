## Building Icon Packs for VueBulma

Icons for VueBulma are built by iterating a directory of SVG files. The aim is to open each file and extract the contents of the `<svg>` node and append it to a JS file that exports it, making it tree-shaking friendly.

The builders are included in VueBulma for when you'd like to update the icon packs, the following of which are supported:

- bytesize-icons
- heroicons-ui
- zondicons
- feather-icons

If you’d like to update these packs, simply clone VueBulma and run `npm ci`. Then, for each pack, run the following command:

```shell
node icons/builders/<name> # where <name> is the name of the pack.
```

This tells Node to run the builder against the dependency, and generate the export-file that VueBulma’s icon library can utilise.

Alternatively, you can rebuild all packs by running:

```shell
npm run build-icons
```
