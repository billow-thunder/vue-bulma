## Colour Picker Components

These components were built by Don (xiaokaike on GitHub) – see https://github.com/xiaokaike/vue-color. They've been modified to suit the needs of VueBulma.
