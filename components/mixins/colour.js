import tinycolor from 'tinycolor2'
import { getColourPayload } from '../../utilities/colour/get-colour-payload'

export let colour = {
  props: ['value'],

  data() {
    return {
      colourValue: getColourPayload(this.value)
    }
  },

  computed: {
    colour: {
      get() {
        return this.colourValue
      },
      set(value) {
        this.colourValue = value
        this.$emit('input', value)
      }
    }
  },

  watch: {
    value(value) {
      this.colourValue = getColourPayload(value)
    }
  },

  methods: {
    changeColour(data, oldHue) {
      this.oldHue = this.colour.hsl.h
      this.colour = getColourPayload(data, oldHue || this.oldHue)
    },

    paletteUpperCase(palette) {
      return palette.map(c => c.toUpperCase())
    },

    isTransparent(colour) {
      return tinycolor(colour).getAlpha() === 0
    }
  }
}
