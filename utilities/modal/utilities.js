'use strict'

export function noop() { }

export const closeEvent = 'modal:close'
export const errorEvent = 'modal:error'

export const transitionGroupProps = {
  appear: Boolean,
  appearActiveClass: String,
  appearClass: String,
  appearToClass: String,
  css: Boolean,
  duration: Number | String | Object,
  enterActiveClass: String,
  enterClass: String,
  enterToClass: String,
  leaveActiveClass: String,
  leaveClass: String,
  leaveToClass: String,
  mode: String,
  moveClass: String,
  tag: String,
  type: String,
}

export function collectProps(props, args) {
  if (props.length === 0 && args[0] && typeof args[0] === 'object') {
    return args[0]
  }

  return props.reduce((propsData, prop, i) => {
    propsData[prop] = args[i]
    return propsData
  }, {})
}

export function isComponentCtor(object) {
  if (object != null) {
    const type = typeof object
    if (type === 'object') {
      return (
        typeof object.then === 'function' ||
        typeof object.render === 'function' ||
        typeof object.template === 'string'
      )
    } else if (type === 'function') {
      return typeof object.cid === 'number'
    }
  }

  return false
}

export function filterUndefinedProps(props, component) {
  const propKeys = {}
  const queue = [component]

  function pushIfNotNull(value) {
    if (value != null) {
      queue.push(value)
    }
  }

  // eslint-disable-next-line no-cond-assign
  while (component = queue.shift()) {
    let propKeysArray
    if (Array.isArray(component.props)) {
      propKeysArray = component.props
    } else if (typeof component.props === 'object') {
      propKeysArray = Object.keys(component.props)
    }

    if (propKeysArray) {
      propKeysArray.forEach(key => {
        propKeys[key] = true
      })
    }

    pushIfNotNull(component.options)
    pushIfNotNull(component.extends)

    if (Array.isArray(component.mixins)) {
      component.mixins.forEach(pushIfNotNull)
    }
  }

  return props.filter(key => !propKeys[key])
}
