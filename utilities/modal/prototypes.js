import { create as useModal } from './create'
import Alert from '../../modals/Alert'
import Confirm from '../../modals/Confirm'

export default (Vue) => {
  Vue.prototype.$alert = useModal(Alert)
  Vue.prototype.$confirm = useModal(Confirm)
}
