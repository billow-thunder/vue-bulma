'use strict'

import {
  noop,
  closeEvent,
  errorEvent,
  collectProps,
  transitionGroupProps
} from './utilities'

export const wrappers = {}

export let ModalWrapper = {
  name: 'ModalWrapper',

  props: {
    name: {
      type: String,
      default: 'default',
      validator: value => value
    },
    transitionName: String,
    ...transitionGroupProps
  },

  data: () => ({
    id: 0,
    modals: {}
  }),

  computed: {
    modalIds() {
      return Object.keys(this.modals)
    }
  },

  created() {
    if (process.env.NODE_ENV === 'development' && wrappers[this.name]) {
      console.error(`Modal wrapper ['${this.name}'] may only be declared once.`)
    }
    wrappers[this.name] = this
  },

  beforeDestroy() {
    wrappers[this.name] = undefined
  },

  render(createElement) {
    const $listeners = { ...this.$listeners }

    const $afterLeave = $listeners['after-leave'] || noop
    $listeners['after-leave'] = modalWrapper => {
      modalWrapper.$afterLeave()
      $afterLeave(modalWrapper)
    }

    const $props = {
      ...this.$options.propsData,
      name: this.transitionName
    }

    const $children = this.modalIds.map(modalId => {
      const data = this.modals[modalId]

      const listeners = {}
      listeners[closeEvent] = data.close
      listeners[errorEvent] = data.error

      return createElement(data.component, {
        on: listeners,
        key: data.id,
        props: data.propsData
      })
    })

    const groupPayload = {
      on: $listeners,
      props: $props,
      tag: 'div',
      class: 'modal-wrapper'
    }

    return createElement('transition-group', groupPayload, $children)
  },

  methods: {
    add(modalData, args) {
      const id = this.id++
      let close, error

      let removeAndReturn = data => {
        this.remove(id)
        return data
      }

      let removeAndThrow = reason => {
        this.remove(id)
        throw reason
      }

      const dataPromise = new Promise((resolve, reject) => {
        close = resolve;
        error = reject
      })
        .then(removeAndReturn)
        .catch(removeAndThrow)

      const instancePromise = new Promise(resolve => {
        modalData.createdCallback = resolve
      })

      const transitionPromise = instancePromise
        .then(component => new Promise(resolve => {
          component.$el.$afterLeave = resolve
        }))
        .then(() => dataPromise)

      const finalPromise = modalData.component.then(component => {
        const propsData = {
          arguments: args,
          ...collectProps(modalData.props, args)
        }

        const renderOptions = Object.freeze({
          id, propsData, component, close, error
        })

        this.$set(this.modals, id, renderOptions)

        return dataPromise
      })

      return Object.assign(finalPromise, {
        close,
        error,
        transition: () => transitionPromise,
        getInstance: () => instancePromise
      })
    },

    remove(id) {
      this.$delete(this.modals, id)
    }
  }
}
