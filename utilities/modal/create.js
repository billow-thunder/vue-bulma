'use strict'

import { wrappers } from './wrapper'
import {
  noop,
  closeEvent,
  errorEvent,
  isComponentCtor,
  filterUndefinedProps
} from './utilities'

function makeModalData(props, component) {
  let modalData

  return modalData = {
    props,
    createdCallback: noop,
    component: Promise.resolve(component).then(component => ({
      extends: component.default || component,
      props: filterUndefinedProps(['arguments', ...props], component),

      created() {
        modalData.createdCallback(this)
      },

      methods: {
        $close(data) {
          this.$emit(closeEvent, data)
        },

        $error(data) {
          this.$emit(errorEvent, data)
        }
      }
    }))
  }
}

export function create(options, ...props) {
  if (options == null) {
    if (process.env.NODE_ENV === 'development') {
      console.error('Modal options cannot be null or undefined on create()')
    }
    return null
  }

  let wrapper = 'default'
  let component = options

  if (isComponentCtor(options.component)) {
    component = options.component
    wrapper = options.wrapper || wrapper
    props = options.props || []
  } else if (!isComponentCtor(options)) {
    if (process.env.NODE_ENV === 'development') {
      console.error('A modal component constructor is required in create()')
    }
    return null
  }

  const modalData = makeModalData(props, component)

  return function modalFunction(...args) {
    if (wrappers[wrapper]) {
      return wrappers[wrapper].add(modalData, args)
    } else {
      if (process.env.NODE_ENV === 'development') {
        console.error(`Modal wrapper [${wrapper}] does not exist. To resolve, add <modal-wrapper name="${wrapper}" /> to the root of the DOM tree.`)
      }
      return Promise.reject(new TypeError(`Undefined reference to wrapper ${wrapper}`))
    }
  }
}
