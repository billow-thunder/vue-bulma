import tinycolor from 'tinycolor2'

/**
 * Given a colour, find the monochromatic invert.
 * @param {any} colour
 */
export let getColourInvert = colour => {
  colour = tinycolor(colour)
  if (!colour.isValid()) {
    throw new Error('The provided colour is not valid.')
  }

  let [red, green, blue] = colour.toHex().split(/(.{2})/)
    .map(part => parseInt(part, 16) || false)
    .filter(part => part)

  return tinycolor(
    (red * 0.299 + green * 0.587 + blue * 0.114) > 186
      ? '#000'
      : '#fff'
  )
}
