import tinycolor from 'tinycolor2'

/**
 * Build and return the colour payload.
 * @param {Object} data
 * @param {Number} oldHue
 * @return {Object}
 */
export let getColourPayload = (data, oldHue) => {
  let alpha = data && data.a, colour

  if (data && data.hsl) {
    colour = tinycolor(data.hsl)
  } else if (data && data.hex && data.hex.length > 0) {
    colour = tinycolor(data.hex)
  } else {
    colour = tinycolor(data)
  }

  if (colour && (colour._a === undefined || colour._a === null)) {
    colour.setAlpha(alpha || 1)
  }

  let hsl = colour.toHsl(), hsv = colour.toHsv()

  if (hsl.s === 0) {
    hsv.h = hsl.h = data.h || (data.hsl && data.hsl.h) || oldHue || 0
  }

  return {
    hsl,
    hsv,
    hex: colour.toHexString().toUpperCase(),
    hex8: colour.toHex8String().toUpperCase(),
    rgba: colour.toRgb(),
    oldHue: data.h || oldHue || hsl.h,
    source: data.source,
    a: data.a || colour.getAlpha()
  }
}
