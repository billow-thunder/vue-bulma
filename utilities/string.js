export let random = (length = 6) => {
  let symbols = 'abcdef0123456789';
  return Array.from(
    { length },
    () => symbols.charAt(Math.floor(Math.random() * symbols.length))
  ).join('')
}

export let slug = input => {
  let specials = {
    chars: 'àáäâãèéëêìíïîòóöôùúüûñçßÿœæŕśńṕẃǵǹḿǘẍźḧ·/_,:;',
    replace: 'aaaaaeeeeiiiioooouuuuncsyoarsnpwgnmuxzh------',
  },
    specialCharsPiped = new RegExp(specials.chars.split('').join('|'), 'g')

  return input.toString().toLowerCase()
    .replace(/\s+/g, '-')
    .replace(specialCharsPiped, c => specials.replace.charAt(specials.chars.indexOf(c)))
    .replace(/&/g, '')
    .replace(/[^\w\-]+/g, '')
    .replace(/\-\-+/g, '-')
    .replace(/^-+/, '')
    .replace(/-+$/, '')
}

export let camel = input => {
  let [first, ...acc] = input.replace(/[^\w\d]/g, ' ').split(/\s+/)
  return first.toLowerCase() + acc.map(x => x.charAt(0).toUpperCase()
    + x.slice(1).toLowerCase()).join('')
}

export let format = (input, replacement) => {
  return input.replace(
    /:([a-zA-z0-9$-]+)|{((?:(?!{|}).)+)}/g,
    (...matches) => replacement[matches[1] || matches[2]] || matches[0]
  )
}

export let basicTags = (input) => {
  input = input.replace(/\*(.*?)\*/g, '&#8203;<span class="has-text-weight-bold">$1</span>&#8203;');
  input = input.replace(/_(.*?)_/g, '&#8203;<span class="is-italic">$1</u>&#8203;');
  input = input.replace(/~(.*?)~/g, '&#8203;<strike>$1</strike>&#8203;');
  return input
}

export default {
  random,
  slug,
  camel,
  format,
  basicTags
}
