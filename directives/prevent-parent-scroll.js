let map = new Map()

export default {
  bind(element) {
    let handler = event => {
      if (
        (element.scrollTop === 0 && event.deltaY < 0) ||
        (Math.abs(element.scrollTop - (element.scrollHeight - element.clientHeight)) <= 1 &&
          event.deltaY > 0)
      ) event.preventDefault()
    }

    map.set(element, handler)
    element.addEventListener('wheel', handler)
  },

  unbind(el) {
    el.removeEventListener('wheel', map.get(el))
    map.delete(el)
  }
}
