// Lodash
import defaults from 'lodash/defaultsDeep'
import each from 'lodash/forEach'

// Config & Installers
import defaultConfig from './config'
import installComponents from './installers/components'
import installThirdParties from './installers/third-parties'
import installModalPrototypes from './utilities/modal/prototypes'

// Models and Stores
import storeModules from './store/modules'

// Directives
import PreventParentScroll from './directives/prevent-parent-scroll'
import InputAutowidth from './directives/input-autowidth'

// Installation routine
let install = (app, options = {}) => {

  // Assign config and add to Vue prototype
  let mergedConfig = defaults(options.config, defaultConfig)
  app.prototype.$vueBulma = mergedConfig

  // Directives
  app.directive('prevent-parent-scroll', PreventParentScroll)
  app.directive('autowidth', InputAutowidth)

  // Using requireContext, install all components and third party
  // components/directives.
  installComponents(app, mergedConfig.components.exclusions || [])
  installThirdParties(app, mergedConfig.thirdParty)

  // Prototypes
  installModalPrototypes(app)

  // If a store is passed, register modules defined by config.
  if (options.store && options.storeModules && options.store.registerModule) {
    each(options.storeModules, store => {
      if (storeModules[store]) {
        options.store.registerModule(store, storeModules[store])
      }
    })
  }
}

// Exports
export default install
export { create as useModal } from './utilities/modal/create'
