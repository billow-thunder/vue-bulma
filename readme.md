# Vue Bulma, by Billow

[![Vue](https://forthebadge.com/images/badges/made-with-vue.svg)](https://vuejs.org)
[![forthebadge](https://forthebadge.com/images/badges/gluten-free.svg)](https://forthebadge.com)

**Current Release:** 3.0.x ([changelog](https://gitlab.com/billow-thunder/vue-bulma/blob/master/changelog.md))

A growing collection of Vue components, styled with [Bulma](https://bulma.io).

## Installation

```shell
npm i @billow/vue-bulma
```

## Usage

```js
import VueBulma from '@billow/vue-bulma'
Vue.use(VueBulma)
```

Additional configuration can be passed in, using the `config` property:

```js
Vue.use(VueBulma, {
  config: {
    //
  }
})
```

To see what configuration can be passed in, feel free to take a look at the **[default configuration](https://gitlab.com/billow-thunder/vue-bulma/blob/master/config.js)**.

A `store` is also accepted, but doesn’t do anything at the moment. It’s reserved for the time being, and may even be removed in the future, unless a global Error Bag is brought into the picture.

## Stylesheet

You’ll need to import the Vue Bulma stylesheet, which adds on top of the existing Bulma stylesheet, providing support to the components provided.

```scss
@import "~@billow/vue-bulma/styles";
```

If, however, you’d like to change some defaults, you can first import the `initial-variables` stylesheet, then make your changes, then import the main stylesheet:

```scss
@import "~@billow/vue-bulma/scss/initial-variables";

// … Change some things …

@import "~@billow/vue-bulma/styles";
```

## Upgrading from 2.1

To a large degree, the 3.0.x API is the same as that of 2.1.x and even 2.x. Whilst primarily targeted at new apps, this is a recommended upgrade for priority production apps. For a short while, 2.1.x will continue to be maintained on the `legacy/2.1` branch.

**[Here’s an upgrade guide](https://gitlab.com/billow-thunder/vue-bulma/blob/master/upgrading.md)** to follow when upgrading existing apps from 2.1.x. If you’re upgrading from 2.0.x, then you’ll need to follow the upgrade guide in `legacy/2.1` first.

## Example-Driven Docs (WIP)

Learn how to use VueBulma at https://vue-bulma.billow.co.za.
