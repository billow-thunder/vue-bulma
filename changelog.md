# v3 Changelog

## 3.0.0 • Major Release • Sunday, 10 November 2019

⚠️ **Contains Breaking Changes.** See the [upgrade guide](./upgrading.md) for details.

#### Added

- Avatars are now presence-aware, and additionally support identicons and colorization.
- Basic text formatting added, driven by `basicTags`.
- Better tooltip support – powered by `v-tooltip`, replacing `bulma-tooltip`.
- Data selector now supports the addition of new tags via `@tag`, `async` search, as well as the `autofocus`, `autocomplete`, `role` and `debounce` props.
- Date picker now supports opt-in/out dates, via `enabled-dates` and `disabled-dates`, respectively. It also supports the `onDayCreate` and `onReady` callbacks.
- First-class support for promise-based modals, thanks to `vue-modal-dialogs` (thank you, [Jingkun Hua](https://github.com/hjkcai)).
- Persona component added, adding capabilities to Avatar.
- Popover support, also powered by `v-tooltip`.
- Tag component added.

#### Changed

- All stylesheets are now SCSS based (no longer SASS).
- All slots are now scoped slots by default.
- `BaseModal` renamed to `Modal`.
- `Modal`.`close-button` (of type String) changed to `Modal`.`close-button-outside` (of type Boolean).
- `Modal`.`footer-class` now defaults to `is-reversed`.
- `Pager`.`details-class` now defaults to `is-scheme-main`
- `Pager`.`format` changed to `Page *:current* of *:last* (:total :totalContext)`.
- `Pager`.`context` changed from 'record' to 'item'
- `Pager`.`working-text` changed from 'Please wait, loading data…' to 'Loading…'
- `DataSelector` computed items are no longer component-computed, but rather handled manually.

#### Removed

- Dependencies: [bulma-tooltip](https://www.npmjs.com/package/bulma-tooltip), [bulma-pricingtable](https://www.npmjs.com/package/bulma-pricingtable), [bulma-divider](https://www.npmjs.com/package/bulma-divider), [bulma-slider](https://www.npmjs.com/package/bulma-slider), [bulma-badge](https://www.npmjs.com/package/bulma-badge)
- Peer Dependency: @billow/js-helpers
- Components: `SliderInput`
- Component Props: `badge`
