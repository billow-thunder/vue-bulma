export default {
  // Icons
  icons: {
    pack: 'fa',
    // Other packs must also be registered here. Ex:
    // zondicons
    // bytesize
    // heroicons
    // feather
  },

  //  Defaults for each component
  components: {
    // Components to skip
    exclusions: [],

    // <field-label/>
    fieldLabel: {
      hintIconPack: 'fa',
      hintIconType: 'fas',
      hintIcon: 'info-circle',
    },

    // <action-button/>
    actionButton: {
      dropdownIconPack: 'fa',
      dropdownIcon: 'angle-down'
    },

    // <pager/>
    pager: {
      buttonIconPack: 'fa',
      buttonIconType: 'fas',
      nextButtonIcon: 'angle-right',
      prevButtonIcon: 'angle-left',
      lastButtonIcon: 'angle-double-right',
      firstButtonIcon: 'angle-double-left'
    },

    // <masked-text-input/>
    maskedTextInput: {
      options: {
        placeholder: "\u2007",
        rightAlign: false,
      }
    }
  },

  // Third party defaults
  thirdParty: {
    // v-tooltip directive; v-popover component
    tooltip: {
      defaultTemplate: '<div class="tooltip-container" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
      popover: {
        defaultBaseClass: 'tooltip-container popover',
      }
    }
  }
}
